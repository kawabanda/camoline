$(document).ready(function() {
  $('.select2').each(function(i, e) {
    select = $(e);
 
    if (select.hasClass('ajax')) {
      select.select2({
        placeholder: select.data('placeholder'),
        ajax: {
          url: select.data('source'),
          dataType: 'json',
          data: function(term, page) {
            return {
              q: {
                name_cont: term
              }
            };
          },
          results: function(data, page) {
            var myResults = [];
            $.each(data, function (index, item) {
                myResults.push({
                    'id': item.id,
                    'text': item.name
                });
            });
            return {
                results: myResults
            };
          },
          cache: true
        },
        initSelection: function(element, callback) {
          var id = $(element).val();
          if (id !== "") {
            $.ajax(select.data('source')+ "/" + id, {
              dataType: "json"
            }).done(function(data) { 
            	callback({'id': data.id, 'text': data.name}); 
            });
          }
        },
		dropdownCssClass: "bigdrop"
      });
    }
  });
});

