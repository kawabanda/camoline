class Core::CompanyController < ApplicationController
  load_and_authorize_resource
  before_action :set_company, only: [:show, :edit, :update]

  # GET /company
  def show
  end

  # GET /company/edit
  def edit
    @company = Core::Company.first
    @currencies = Core::Currency.all
  end

  # POST /company
  def update
    @currencies = Core::Currency.all
    respond_to do |format|
      if @company.update(company_params)
        format.html { redirect_to company_path, notice: t(:company_updated) }
        format.json { render :show, status: :ok, location: @company }
      else
        format.html { render :edit }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

   private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Core::Company.first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:core_company).permit(:name, :full_name, :inn, :kpp, :currency_code)
    end
end
