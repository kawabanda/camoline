class Core::ContractorsController < ApplicationController
  before_action :set_contractor, only: [:show, :edit, :update, :destroy]

  # GET /core/contractors
  # GET /core/contractors.json
  def index
    @search = Core::Contractor.search(params[:q])
    @contractors = @search.result.page(params[:page])

    respond_to do |format|
      format.html
      format.json { render json: @contractors }
    end
  end

  # GET /core/contractors/1
  # GET /core/contractors/1.json
  def show
    respond_to do |format|
      format.html
      format.json { render json: @contractor }
    end
  end

  # GET /core/contractors/new
  def new
    @contractor = Core::Contractor.new
  end

  # GET /core/contractors/1/edit
  def edit
  end

  # POST /core/contractors
  # POST /core/contractors.json
  def create
    @contractor = Core::Contractor.new(contractor_params)

    respond_to do |format|
      if @contractor.save
        format.html { redirect_to contractors_path, notice: t(:contractor_created) }
        format.json { render :show, status: :created, location: @contractor }
      else
        format.html { render :new }
        format.json { render json: @contractor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /core/contractors/1
  # PATCH/PUT /core/contractors/1.json
  def update
    respond_to do |format|
      if @contractor.update(contractor_params)
        format.html { redirect_to contractors_path, notice: t(:contractor_updated) }
        format.json { render :show, status: :ok, location: @contractor }
      else
        format.html { render :edit }
        format.json { render json: @contractor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /core/contractors/1
  # DELETE /core/contractors/1.json
  def destroy
    @contractor.destroy
    respond_to do |format|
      format.html { redirect_to contractors_path, notice: t(:contractor_destroyed) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contractor
      @contractor = Core::Contractor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contractor_params
      params.require(:core_contractor).permit(:name)
    end
end
