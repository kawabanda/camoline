class Core::CurrencyRatesController < ApplicationController
  before_action :set_variables, except: :destroy
  before_action :set_currency_rate, only: [:show, :edit, :update, :destroy]

  # GET /currency_rates
  # GET /currency_rates.json
  def index
    @search = Core::CurrencyRate.search(params[:q])
    @currency_rates = @search.result.page(params[:page])
  end

  # GET /currency_rates/1
  # GET /currency_rates/1.json
  def show
  end

  # GET /currency_rates/new
  def new
    @currency_rate = Core::CurrencyRate.new
  end

  # GET /currency_rates/1/edit
  def edit
  end

  # POST /currency_rates
  # POST /currency_rates.json
  def create
    @currency_rate = Core::CurrencyRate.new(currency_rate_params)

    respond_to do |format|
      if @currency_rate.save
        format.html { redirect_to currency_rates_path, notice: t(:currency_rate_created) }
        format.json { render :show, status: :created, location: @currency_rate }
      else
        format.html { render :new }
        format.json { render json: @currency_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /currency_rates/1
  # PATCH/PUT /currency_rates/1.json
  def update
    respond_to do |format|
      if @currency_rate.update(currency_rate_params)
        format.html { redirect_to currency_rates_path, notice: t(:currency_rate_updated) }
        format.json { render :show, status: :ok, location: @currency_rate }
      else
        format.html { render :edit }
        format.json { render json: @currency_rate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /currency_rates/1
  # DELETE /currency_rates/1.json
  def destroy
    @currency_rate.destroy
    respond_to do |format|
      format.html { redirect_to currency_rates_path, notice: t(:currency_rate_destroyed) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_currency_rate
      @currency_rate = Core::CurrencyRate.find(params[:id])
    end

    def set_variables
      @currencies = Core::Currency.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def currency_rate_params
      params.require(:core_currency_rate).permit(:currency_code, :day, :rate)
    end
end
