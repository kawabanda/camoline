class Core::ProductUnitOfMeasuresController < ApplicationController
  load_and_authorize_resource
  before_action :set_product
  before_action :set_variables, except: :destroy
  before_action :set_product_unit_of_measure, only: [:show, :edit, :update, :destroy]

  # GET /product_unit_of_measures
  # GET /product_unit_of_measures.json
  def index
    @search = @product.product_unit_of_measures.search(params[:q])
    @product_unit_of_measures = @search.result.page(params[:page])
  end

  # GET /product_unit_of_measures/1
  # GET /product_unit_of_measures/1.json
  def show
  end

  # GET /product_unit_of_measures/new
  def new
    @product_unit_of_measure = @product.product_unit_of_measures.new
  end

  # GET /product_unit_of_measures/1/edit
  def edit
  end

  # POST /product_unit_of_measures
  # POST /product_unit_of_measures.json
  def create
    @product_unit_of_measure = @product.product_unit_of_measures.new(product_unit_of_measure_params)

    respond_to do |format|
      if @product_unit_of_measure.save
        format.html { redirect_to product_product_unit_of_measures_path(@product), notice: t(:product_unit_of_measure_created) }
        format.json { render :show, status: :created, location: @product_unit_of_measure }
      else
        format.html { render :new }
        format.json { render json: @product_unit_of_measure.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /product_unit_of_measures/1
  # PATCH/PUT /product_unit_of_measures/1.json
  def update
    respond_to do |format|
      if @product_unit_of_measure.update(product_unit_of_measure_params)
        format.html { redirect_to product_product_unit_of_measures_path(@product), notice: t(:product_unit_of_measure_updated) }
        format.json { render :show, status: :ok, location: @product_unit_of_measure }
      else
        format.html { render :edit }
        format.json { render json: @product_unit_of_measure.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /product_unit_of_measures/1
  # DELETE /product_unit_of_measures/1.json
  def destroy
    @product_unit_of_measure.destroy
    respond_to do |format|
      format.html { redirect_to product_product_unit_of_measures_path(@product), notice: t(:product_unit_of_measure_destroyed) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product_unit_of_measure
      @product_unit_of_measure = @product.product_unit_of_measures.find(params[:id])
    end

    def set_product
      @product = Core::Product.find(params[:product_id])
    end

    def set_variables
      @unit_of_measures = Core::UnitOfMeasure.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_unit_of_measure_params
      params.require(:core_product_unit_of_measure).permit(:unit_of_measure_id, :quantity)
    end
end
