class Core::WarehousesController < ApplicationController
  before_action :set_warehouse, only: [:show, :edit, :update, :destroy]

  # GET /core/warehouses
  # GET /core/warehouses.json
  def index
    @search = Core::Warehouse.search(params[:q])
    @warehouses = @search.result.page(params[:page])

    respond_to do |format|
      format.html
      format.json { render json: @warehouses }
    end
  end

  # GET /core/warehouses/1
  # GET /core/warehouses/1.json
  def show
    respond_to do |format|
      format.html
      format.json { render json: @warehouse }
    end
  end

  # GET /core/warehouses/new
  def new
    @warehouse = Core::Warehouse.new
  end

  # GET /core/warehouses/1/edit
  def edit
  end

  # POST /core/warehouses
  # POST /core/warehouses.json
  def create
    @warehouse = Core::Warehouse.new(warehouse_params)

    respond_to do |format|
      if @warehouse.save
        format.html { redirect_to warehouses_path, notice: t(:warehouse_created) }
        format.json { render :show, status: :created, location: @warehouse }
      else
        format.html { render :new }
        format.json { render json: @warehouse.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /core/warehouses/1
  # PATCH/PUT /core/warehouses/1.json
  def update
    respond_to do |format|
      if @warehouse.update(warehouse_params)
        format.html { redirect_to warehouses_path, notice: t(:warehouse_updated) }
        format.json { render :show, status: :ok, location: @warehouse }
      else
        format.html { render :edit }
        format.json { render json: @warehouse.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /core/warehouses/1
  # DELETE /core/warehouses/1.json
  def destroy
    @warehouse.destroy
    respond_to do |format|
      format.html { redirect_to warehouses_path, notice: t(:warehouse_destroyed) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_warehouse
      @warehouse = Core::Warehouse.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def warehouse_params
      params.require(:core_warehouse).permit(:name)
    end
end
