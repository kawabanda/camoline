class Purchases::PurchasesController < ApplicationController
  before_action :set_variables, except: :destroy
  before_action :set_purchase, only: [:show, :edit, :update, :destroy]

  # GET /purchases
  # GET /purchases.json
  def index
    @search = Purchases::Purchase.search(params[:q])
    @purchases = @search.result.page(params[:page])
  end

  # GET /purchases/1
  # GET /purchases/1.json
  def show
  end

  # GET /purchases/new
  def new
    @purchase = Purchases::Purchase.new
  end

  # GET /purchases/1/edit
  def edit
  end

  # POST /purchases
  # POST /purchases.json
  def create
    @purchase = Purchases::Purchase.new(purchase_params)

    klass = Core::PurchasesSettings.purchase_number_generator.split("::").inject(Object) { |k,n| k.const_get(n) }
    generator = klass.new
    generator.on(Purchases::Purchase)
    @purchase.number = generator.next

    respond_to do |format|
      if @purchase.save
        format.html { redirect_to purchases_path, notice: t(:purchase_created) }
        format.json { render :show, status: :created, location: @purchase }
      else
        format.html { render :new }
        format.json { render json: @purchase.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /purchases/1
  # PATCH/PUT /purchases/1.json
  def update

    respond_to do |format|
      if @purchase.update(purchase_params)
        format.html { redirect_to purchases_path, notice: t(:purchase_updated) }
        format.json { render :show, status: :ok, location: @purchase }
      else
        format.html { render :edit }
        format.json { render json: @purchase.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /purchases/1
  # DELETE /purchases/1.json
  def destroy
    @purchase.destroy
    respond_to do |format|
      format.html { redirect_to purchases_path, notice: t(:purchase_destroyed) }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_purchase
      @purchase = Purchases::Purchase.find(params[:id])
    end

    def set_variables
      @contractors = Core::Contractor.all
      @warehouses = Core::Warehouse.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def purchase_params
      params.require(:purchases_purchase).permit(:contractor_id, :warehouse_id)
    end
end
