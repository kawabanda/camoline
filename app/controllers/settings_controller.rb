class SettingsController < ApplicationController
  def show
  end

  def update
    Core::ModulesSettings[:warehouses] = params[:warehouses]
    @errors = []
    if @errors.count == 0
      redirect_to settings_path, notice: t(:settings_updated)
    else
      render action: "show"
    end
  end
end
