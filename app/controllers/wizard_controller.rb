class WizardController < Wicked::WizardController
  before_action :set_steps
  steps :company, :modules

  def show
    case step
    when :company
      if Company.first
        @company = Company.first
        @currencies = Currency.all
      else
        @company = Company.new
      end
    when :modules
    end
    render_wizard
  end

  def update
    case step
    when :company
      @company = Company.first
      @currencies = Currency.all
      if @company.update(company_params)
        redirect_to next_wizard_path, notice: t(:company_updated)
      else
        render step
      end
    when :modules
      Core::ModulesSettings[:warehouses] = params[:warehouses]
      @errors = []
      if @errors.count == 0
        redirect_to next_wizard_path, notice: t(:settings_updated)
      else
        render step
      end
    end
  end

  private
  def set_steps
    if Core::ModulesSettings.warehouses
      self.steps << :warehouses
    end
  end

  private
  def company_params
    params.require(:company).permit(:name, :full_name, :inn, :kpp, :currency_code)
  end
end
