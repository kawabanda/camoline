class Core::Company < ActiveRecord::Base
  self.primary_key = :inn, :kpp
  validates :name, :full_name, :inn, :kpp, :currency, presence: true
  validates :inn, numericality: {greater_than: 0}
  validates :inn, length: { is: 10 }
  validates :kpp, numericality: {greater_than: 0}
  validates :kpp, length: { is: 9 }
  belongs_to :currency, foreign_key: "currency_code", class_name: "Core::Currency"
end
