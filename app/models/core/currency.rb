class Core::Currency < ActiveRecord::Base
  self.primary_key = :code
  validates :name, :code, presence: true
  has_many :money, foreign_key: "currency_code"
  has_many :currency_rates, dependent: :destroy, foreign_key: "currency_code", class_name: "Core::CurrencyRate"
  has_many :companies, foreign_key: "currency_code", class_name: "Core::Company"
end
