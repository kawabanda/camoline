class Core::CurrencyRate < ActiveRecord::Base
  validates :day, :currency, :rate, presence: true
  validates :rate, numericality: {greater_than: 0}
  validates :currency, uniqueness: { scope: :day, message: "One currency rate per day" }
  belongs_to :currency, foreign_key: "currency_code", class_name: "Core::Currency"
end
