module Core::Generators::Generator
  def on(active_record)
  	raise NotImplementedError
  end

  def next
  	raise NotImplementedError
  end
end