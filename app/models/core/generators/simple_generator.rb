class Core::Generators::SimpleGenerator 
  include Core::Generators::Generator

  @active_record
  def on(active_record)
  	@active_record = active_record
  end

  def next
  	next_number = 1
    document = @active_record.last
    if document
      next_number = document.number.to_i + 1      
    end

    return next_number
  end
end