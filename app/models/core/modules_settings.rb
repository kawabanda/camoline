class Core::ModulesSettings < Settingslogic
  source "#{Rails.root}/config/modules_settings.yml"
  namespace Rails.env
end