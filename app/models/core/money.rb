class Core::Money < ActiveRecord::Base
  validates :currency, :amount, presence: true
  validates :amount, numericality: {greater_than: 0}
  belongs_to :currency, foreign_key: "currency_code", class_name: "Core::Currency"
end
