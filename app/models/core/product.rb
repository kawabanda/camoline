class Core::Product < ActiveRecord::Base
  validates :name, :article, presence: true
  has_many :product_unit_of_measures, dependent: :destroy, class_name: "Core::ProductUnitOfMeasure"
  has_many :unit_of_measures, through: :product_unit_of_measures, class_name: "Core::UnitOfMeasure"
end
