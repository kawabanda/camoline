class Core::ProductUnitOfMeasure < ActiveRecord::Base
  validates :product, :unit_of_measure, :quantity, presence: true
  validates :unit_of_measure_id, :uniqueness => { :scope => :product_id,
    :message => I18n.t(:once_per_product) }
  validates :quantity, numericality: {greater_than: 0}
  belongs_to :product, class_name: "Core::Product"
  belongs_to :unit_of_measure, class_name: "Core::UnitOfMeasure"
end
