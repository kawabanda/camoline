class Core::PurchasesSettings < Settingslogic
  source "#{Rails.root}/config/purchases.yml"
  namespace Rails.env
end