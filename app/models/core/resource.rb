class Core::Resource < ActiveRecord::Base
  validates :name, presence: true
end
