class Core::Service < ActiveRecord::Base
  validates :name, presence: true
end
