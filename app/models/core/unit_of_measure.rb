class Core::UnitOfMeasure < ActiveRecord::Base
  validates :name, presence: true
  validates :name, :uniqueness => { :case_sensitive => false }
  has_many :product_unit_of_measure, dependent: :destroy, class_name: "Core::productUnitOfMeasure"
  has_many :products, through: :product_unit_of_measures, class_name: "Core::Product"
end
