class Core::Warehouse < ActiveRecord::Base
  validates :name, presence: true
  has_many :purchases, class_name: "Purchases::Purchase"
end
