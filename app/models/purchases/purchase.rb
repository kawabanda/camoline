class Purchases::Purchase < ActiveRecord::Base
  self.primary_key = 'number'
  validates :number, :warehouse, :contractor, presence: true
  validates :number, :uniqueness => { :case_sensitive => false }
  belongs_to :contractor, class_name: "Core::Contractor"
  belongs_to :warehouse, class_name: "Core::Warehouse"
end
