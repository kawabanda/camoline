class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable, :lockable
  has_and_belongs_to_many :roles
  validate :check_has_minimum_one_role

  def role?(role)
    return self.roles.find_by_name(role).try(:name) == role.to_s
  end

  def check_has_minimum_one_role
    if self.roles.empty?
      self.errors.add(:role, I18n.t('errors.messages.must_be_selected'))
    end
  end
end
