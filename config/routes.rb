Rails.application.routes.draw do

  scope module: :core do
    resources :contractors
    resources :warehouses
    resources :products do
      resources :product_unit_of_measures
    end
    resources :unit_of_measures
    resources :services
    resources :resources
    resources :money
    resources :currency_rates
  end

  scope module: :purchases do
    resources :purchases
  end

  devise_for :users

  resources :users do
    member do
      get :edit_password
      put :update_password
    end
  end

  get 'company', to: 'core/company#show'
  get 'company/edit', to: 'core/company#edit'
  put 'company', to: 'core/company#update', as: 'company_update'

  get 'settings', to: 'settings#show'
  put 'settings', to: 'settings#update'

  root 'pages#index'
  get 'bali', to: 'pages#bali'

  resources :wizard
end
