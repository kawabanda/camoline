class CreateCoreProducts < ActiveRecord::Migration
  def change
    create_table :core_products do |t|
      t.string :article
      t.string :name
      t.text :desc

      t.timestamps
    end
  end
end
