class CreateCoreUnitOfMeasures < ActiveRecord::Migration
  def change
    create_table :core_unit_of_measures do |t|
      t.string :name

      t.timestamps
    end
  end
end
