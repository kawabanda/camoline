class CreateCoreProductUnitOfMeasures < ActiveRecord::Migration
  def change
    create_table :core_product_unit_of_measures do |t|
      t.references :product, index: true
      t.references :unit_of_measure, index: true
      t.decimal :quantity, precision: 16, scale: 8

      t.timestamps
    end
  end
end
