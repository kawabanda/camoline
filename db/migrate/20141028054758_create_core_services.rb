class CreateCoreServices < ActiveRecord::Migration
  def change
    create_table :core_services do |t|
      t.string :name
      t.text :desc

      t.timestamps
    end
  end
end
