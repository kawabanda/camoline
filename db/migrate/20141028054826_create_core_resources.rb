class CreateCoreResources < ActiveRecord::Migration
  def change
    create_table :core_resources do |t|
      t.string :name
      t.text :desc

      t.timestamps
    end
  end
end
