class CreateCoreCurrencies < ActiveRecord::Migration
  def change
    create_table :core_currencies, :id => false do |t|
      t.string :code
      t.string :name

      t.timestamps
    end
  end
end
