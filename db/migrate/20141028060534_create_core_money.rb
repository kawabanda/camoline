class CreateCoreMoney < ActiveRecord::Migration
  def change
    create_table :core_money do |t|
      t.string :currency_code
      t.decimal :amount, precision: 16, scale: 8

      t.timestamps
    end
  end
end
