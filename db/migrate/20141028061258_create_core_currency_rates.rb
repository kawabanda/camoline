class CreateCoreCurrencyRates < ActiveRecord::Migration
  def change
    create_table :core_currency_rates do |t|
      t.date :day
      t.string :currency_code
      t.decimal :rate, precision: 16, scale: 8

      t.timestamps
    end
  end
end
