class CreateCoreCompanies < ActiveRecord::Migration
  def change
    create_table :core_companies, :id => false do |t|
      t.string :name
      t.string :full_name
      t.integer :inn
      t.integer :kpp

      t.timestamps
    end
  end
end
