class AddCoreCurrencyToCoreCompany < ActiveRecord::Migration
  def change
    add_column :core_companies, :currency_code, :string
  end
end
