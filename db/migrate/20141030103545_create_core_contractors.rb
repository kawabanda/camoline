class CreateCoreContractors < ActiveRecord::Migration
  def change
    create_table :core_contractors do |t|
      t.string :name

      t.timestamps
    end
  end
end
