class CreateCoreWarehouses < ActiveRecord::Migration
  def change
    create_table :core_warehouses do |t|
      t.string :name

      t.timestamps
    end
  end
end
