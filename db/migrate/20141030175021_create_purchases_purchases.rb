class CreatePurchasesPurchases < ActiveRecord::Migration
  def change
    create_table :purchases_purchases, id: false do |t|
      t.string :number, null: false
      t.references :contractor, index: true
      t.references :warehouse, index: true

      t.timestamps
    end
    add_index :purchases_purchases, :number, unique: true
  end
end
