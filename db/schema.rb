# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141030175021) do

  create_table "core_companies", id: false, force: true do |t|
    t.string   "name"
    t.string   "full_name"
    t.integer  "inn"
    t.integer  "kpp"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "currency_code"
  end

  create_table "core_contractors", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "core_currencies", id: false, force: true do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "core_currency_rates", force: true do |t|
    t.date     "day"
    t.string   "currency_code"
    t.decimal  "rate",          precision: 16, scale: 8
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "core_money", force: true do |t|
    t.string   "currency_code"
    t.decimal  "amount",        precision: 16, scale: 8
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "core_product_unit_of_measures", force: true do |t|
    t.integer  "product_id"
    t.integer  "unit_of_measure_id"
    t.decimal  "quantity",           precision: 16, scale: 8
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "core_product_unit_of_measures", ["product_id"], name: "index_core_product_unit_of_measures_on_product_id"
  add_index "core_product_unit_of_measures", ["unit_of_measure_id"], name: "index_core_product_unit_of_measures_on_unit_of_measure_id"

  create_table "core_products", force: true do |t|
    t.string   "article"
    t.string   "name"
    t.text     "desc"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "core_resources", force: true do |t|
    t.string   "name"
    t.text     "desc"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "core_services", force: true do |t|
    t.string   "name"
    t.text     "desc"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "core_unit_of_measures", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "core_warehouses", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "purchases_purchases", id: false, force: true do |t|
    t.string   "number",        null: false
    t.integer  "contractor_id"
    t.integer  "warehouse_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "purchases_purchases", ["contractor_id"], name: "index_purchases_purchases_on_contractor_id"
  add_index "purchases_purchases", ["number"], name: "index_purchases_purchases_on_number", unique: true
  add_index "purchases_purchases", ["warehouse_id"], name: "index_purchases_purchases_on_warehouse_id"

  create_table "roles", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles_users", id: false, force: true do |t|
    t.integer "user_id", null: false
    t.integer "role_id", null: false
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true

end
