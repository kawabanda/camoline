# Create default root user
role_admin = Role.create(name: 'admin')

user_admin = User.create(email: 'admin@test.com', password: '423200', password_confirmation: '423200')
user_admin.roles << role_admin
user_admin.save

currencies = Core::Currency.create([{code: 'RUB', name: 'Russia Ruble'}, {code: 'USD', name: 'United States Dollar'}, {code: 'EUR', name: 'Euro Member Countries'}])
currency_rub = Core::Currency.find('RUB')
company = Core::Company.create(inn: 1659091192, kpp: 565645164, name: 'Рога и копыта', full_name: 'ООО Рога и копыта', currency_code: currency_rub.code)
