require 'test_helper'

class Core::ContractorsControllerTest < ActionController::TestCase
  setup do
    @core_contractor = core_contractors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:core_contractors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create core_contractor" do
    assert_difference('Core::Contractor.count') do
      post :create, core_contractor: { name: @core_contractor.name }
    end

    assert_redirected_to core_contractor_path(assigns(:core_contractor))
  end

  test "should show core_contractor" do
    get :show, id: @core_contractor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @core_contractor
    assert_response :success
  end

  test "should update core_contractor" do
    patch :update, id: @core_contractor, core_contractor: { name: @core_contractor.name }
    assert_redirected_to core_contractor_path(assigns(:core_contractor))
  end

  test "should destroy core_contractor" do
    assert_difference('Core::Contractor.count', -1) do
      delete :destroy, id: @core_contractor
    end

    assert_redirected_to core_contractors_path
  end
end
