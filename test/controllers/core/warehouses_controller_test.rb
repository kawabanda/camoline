require 'test_helper'

class Core::WarehousesControllerTest < ActionController::TestCase
  setup do
    @core_warehouse = core_warehouses(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:core_warehouses)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create core_warehouse" do
    assert_difference('Core::Warehouse.count') do
      post :create, core_warehouse: { name: @core_warehouse.name }
    end

    assert_redirected_to core_warehouse_path(assigns(:core_warehouse))
  end

  test "should show core_warehouse" do
    get :show, id: @core_warehouse
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @core_warehouse
    assert_response :success
  end

  test "should update core_warehouse" do
    patch :update, id: @core_warehouse, core_warehouse: { name: @core_warehouse.name }
    assert_redirected_to core_warehouse_path(assigns(:core_warehouse))
  end

  test "should destroy core_warehouse" do
    assert_difference('Core::Warehouse.count', -1) do
      delete :destroy, id: @core_warehouse
    end

    assert_redirected_to core_warehouses_path
  end
end
