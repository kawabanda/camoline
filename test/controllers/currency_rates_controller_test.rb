require 'test_helper'

class CurrencyRatesControllerTest < ActionController::TestCase
  setup do
    @currency_rate = currency_rates(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:currency_rates)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create currency_rate" do
    assert_difference('CurrencyRate.count') do
      post :create, currency_rate: {  }
    end

    assert_redirected_to currency_rate_path(assigns(:currency_rate))
  end

  test "should show currency_rate" do
    get :show, id: @currency_rate
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @currency_rate
    assert_response :success
  end

  test "should update currency_rate" do
    patch :update, id: @currency_rate, currency_rate: {  }
    assert_redirected_to currency_rate_path(assigns(:currency_rate))
  end

  test "should destroy currency_rate" do
    assert_difference('CurrencyRate.count', -1) do
      delete :destroy, id: @currency_rate
    end

    assert_redirected_to currency_rates_path
  end
end
